package com.gmail.onex.element.cache;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CacheTest {

    private Cache<Integer, String> cache;

    @Test
    void putIntoCacheThenGet() {
        cache = new Cache<>(EjectionStrategy.LRU, 1);
        cache.put(1, "One");
        String cached = cache.get(1);
        assertEquals("One", cached);
    }

    @Test
    void alreadyCachedItemNotOverwhelmingLRUCacheWhenPut() {
        cache = new Cache<>(EjectionStrategy.LRU, 2);
        cache.put(1, "One");
        cache.put(2, "Two");
        cache.put(2, "Two");

        assertEquals("One", cache.get(1));
        assertEquals("Two", cache.get(2));
    }

    @Test
    void alreadyCachedItemNotOverwhelmingMRUCacheWhenPut() {
        cache = new Cache<>(EjectionStrategy.MRU, 2);
        cache.put(1, "One");
        cache.put(2, "Two");
        cache.put(2, "Two");

        assertEquals("One", cache.get(1));
        assertEquals("Two", cache.get(2));
    }

    @Test
    void lruItemEjectedWhenLRUCacheOverwhelmed() {
        cache = new Cache<>(EjectionStrategy.LRU, 2);
        cache.put(1, "One");
        cache.put(2, "Two");
        cache.put(3, "Three");//this line ejects lru "One" from cache

        assertNull(cache.get(1));
    }

    @Test
    void mruItemEjectedWhenMRUCacheOverwhelmed() {
        cache = new Cache<>(EjectionStrategy.MRU, 2);
        cache.put(1, "One");
        cache.put(2, "Two");
        cache.put(3, "Three");//this line ejects mru "Two" from cache

        assertNull(cache.get(2));
    }

    @Test
    void exceptionWhenSizeIsLessThanOne() {
        Exception exception = assertThrows(RuntimeException.class, () ->
                cache = new Cache<>(EjectionStrategy.LRU, 0));
        assertEquals("Cache size must be equal or greater than 1", exception.getMessage());
    }

    @Test
    void exceptionWhenEjectionStrategyIsNull() {
        Exception exception = assertThrows(RuntimeException.class, () ->
                cache = new Cache<>(null, 2));
        assertEquals("Cache ejection strategy is null", exception.getMessage());
    }

    @Test
    void mruItemUpdatesWhenGet() {
        cache = new Cache<>(EjectionStrategy.MRU, 3);
        cache.put(1, "One");
        cache.put(2, "Two");
        cache.put(3, "Three");
        cache.get(2);//this line makes "Two" mru item
        cache.put(4, "Four");//this line ejects mru item from cache

        assertNull(cache.get(2));

    }

    @Test
    void lruItemUpdatesWhenGet() {
        cache = new Cache<>(EjectionStrategy.LRU, 3);
        cache.put(1, "One");
        cache.put(2, "Two");
        cache.put(3, "Three");

        //This block makes "Three" lru item
        cache.get(1);
        cache.get(2);

        cache.put(4, "Four");//this line ejects lru item from cache

        assertNull(cache.get(3));
    }

}
