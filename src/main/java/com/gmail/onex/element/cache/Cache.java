package com.gmail.onex.element.cache;

import java.util.HashMap;

public class Cache<K, V> {

    private final EjectionStrategy ejectionStrategy;
    private final HashMap<K, CacheItemNode<K, V>> cache;
    private final int maxSize;

    private int size;
    private CacheItemNode<K, V> lruItem;
    private CacheItemNode<K, V> mruItem;

    public Cache(EjectionStrategy ejectionStrategy, int maxSize) {
        if (ejectionStrategy == null) {
            throw new RuntimeException("Cache ejection strategy is null");
        }
        if (maxSize < 1) {
            throw new RuntimeException("Cache size must be equal or greater than 1");
        }

        this.ejectionStrategy = ejectionStrategy;
        this.maxSize = maxSize;
        cache = new HashMap<>(maxSize);
        lruItem = new CacheItemNode<>(null, null, null, null);
        mruItem = lruItem;
    }

    public V get(K key) {
        CacheItemNode<K, V> search = cache.get(key);

        if (search == null) return null;
        if (search == mruItem) return mruItem.getItem();

        CacheItemNode<K, V> previous = search.getPrevious();
        CacheItemNode<K, V> next = search.getNext();

        //Update lru item
        if (search == lruItem) {
            next.setPrevious(null);
            lruItem = next;
        } else {
            previous.setNext(next);
            next.setPrevious(previous);
        }

        //Update mru item
        search.setPrevious(mruItem);
        search.setNext(null);
        mruItem.setNext(search);
        mruItem = search;

        return search.getItem();
    }

    public void put(K key, V item) {
        if (cache.containsKey(key)) return;

        CacheItemNode<K, V> newItem = new CacheItemNode<>(key, item, mruItem, null);
        mruItem.setNext(newItem);
        cache.put(key, newItem);
        mruItem = newItem;

        if (size == maxSize) {

            switch (ejectionStrategy) {
                case LRU:
                    cache.remove(lruItem.getKey());
                    lruItem = lruItem.getNext();
                    lruItem.setPrevious(null);
                    break;
                case MRU:
                    CacheItemNode previousMRU = mruItem.getPrevious();
                    cache.remove(previousMRU.getKey());
                    previousMRU.getPrevious().setNext(newItem);
                    newItem.setPrevious(previousMRU.getPrevious());
            }
        }

        if (size < maxSize) {
            if (size == 0) {
                lruItem = newItem;
            }
            size ++;
        }
    }

    private class CacheItemNode<T, D> {

        private T key;
        private D item;
        private CacheItemNode<T, D> previous;
        private CacheItemNode<T, D> next;

        CacheItemNode(T key, D item, CacheItemNode<T, D> previous, CacheItemNode<T, D> next) {
            this.key = key;
            this.item = item;
            this.previous = previous;
            this.next = next;
        }

        T getKey() {
            return key;
        }

        D getItem() {
            return item;
        }

        CacheItemNode<T, D> getPrevious() {
            return previous;
        }

        void setPrevious(CacheItemNode<T, D> previous) {
            this.previous = previous;
        }

        CacheItemNode<T, D> getNext() {
            return next;
        }

        void setNext(CacheItemNode<T, D> next) {
            this.next = next;
        }

    }

}
