package com.gmail.onex.element.cache;

public enum EjectionStrategy {
    MRU,
    LRU
}
